// Object can be create both with and without data type.
const car: { type: string, model: string, year?: number } = {
    type: "Toyota",
    model: "Corolla", 
}
console.log(car);